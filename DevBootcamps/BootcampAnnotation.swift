//
//  BootcampAnnotation.swift
//  DevBootcamps
//
//  Created by Juan Ramirez on 5/25/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import MapKit

class BootcampAnnotation: NSObject, MKAnnotation {
    
    // Requried from the MKAnnotation protocol
    var coordinate = CLLocationCoordinate2D()
 
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
}