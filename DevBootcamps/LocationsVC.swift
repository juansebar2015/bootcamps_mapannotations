//
//  SecondViewController.swift
//  DevBootcamps
//
//  Created by Juan Ramirez on 5/25/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import MapKit
                                                                                // Has optional functions to map events
class LocationsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    // Radius of the region in map
    let regionRadius: CLLocationDistance = 1000
    
    // Manages what permissions are allowed by the user
    let locationManager = CLLocationManager()
    
    // Lets Pretend these were downloaded from the server
    let addresses = [
        "20433 Via San Marino Cupertino, CA 95014",
        "20650 Homestead Rd, Cupertino, CA 95014",
        "11010 N De Anza Blvd, Cupertino, CA 95014"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        mapView.delegate = self
        
        for add in addresses {
            getPlacemarkFromAddress(add)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        locationAuthStatus()
        
    }

    // UITableView Protocols
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // Check status of location and permissions
    func locationAuthStatus () {
                                                    // Authorize user location tracking when app is in use
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            
            mapView.showsUserLocation = true
            
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
    }
    
    func centerMapOnLocation(location: CLLocation) {
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    // Called whenever a users location is updated
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        
        
        if let loc = userLocation.location {
            //Only center the map if we have an actual location
            centerMapOnLocation(loc)
        }
        
    }
    
    // Customize the pin drops (Caled everytime before a pin is droped to the map)
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        // Check the class of the annotation being droped
        if annotation.isKindOfClass(BootcampAnnotation){
            
            let annoView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Default")
            annoView.pinTintColor = UIColor.blackColor()       //Sets the color
            annoView.animatesDrop = true                    // Animates the drop of the pin
            
            return annoView
            
        } else if annotation.isKindOfClass(MKUserLocation) {    // To not touch the user location
            return nil
        }
        
        return nil
        
    }
    
    
    
    // Convert location into an annotation
    func createAnnotationForLocation(location: CLLocation) {
        let bootcamp = BootcampAnnotation(coordinate: location.coordinate)
        
        mapView.addAnnotation(bootcamp)
        
        /*
         * Before the annotation drops to the map the "func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {" is called
         */
    }
    
    func getPlacemarkFromAddress(address: String) {
        CLGeocoder().geocodeAddressString(address) { (placemarks: [CLPlacemark]?, error: NSError?) in
            
            // Check if there are any locations
            if let marks = placemarks where marks.count > 0 {
                
                // Grab the first object off the array
                if let loc = marks[0].location {
                    //We have a valid location with coordinates (from the strings)
                    
                    self.createAnnotationForLocation(loc)
                }
            }
            
        }
    }
    
}

